<?PHP
	//*********************************************************************************
	//* requete.class.php
	//*********************************************************************************
	/**
	* requete.class.php
	*
	* contient les donnees sur la rubrique et la structure qui vont afficher les donnees
	*
	*
	* @author philippe Guiomar
	* @version $Revision$
	* @date $Date$
	* @package
	* @param void
	* @return void
	*/

class requete {

var $rubrique="";
var $structure="";
var $langue="";
var $secure="";
var $donnees_specifiques;
var $liste_encadres; //obj Encadre
var $liste_encadres_recherche; //obj liste_encadres_recherche

	//*********************************************************************************
	//* requete
	//*********************************************************************************
	/**
	* requete
	*
	* @author philippe Guiomar
	* @version $Revision$
	* @date $Date$
	* @package
	* @param strRubrique : String, strStructure : String
	* @return void
	*/
	function requete($strRubrique="",$strStructure="") {
			$this->setRubrique($strRubrique);				
			$this->setStructure($strStructure);
	}//fin fct 

	function setRubrique($strRubrique) {
			$this->rubrique=$strRubrique;
	}//fin fct

	function setLangue($strLangue) {
			$this->langue=$strLangue;
	}//fin fct

	function setSecure($strSecure) {
			$this->secure=$strSecure;
	}//fin fct

	function setStructure($strStructure) {
			$this->structure=$strStructure;
	}//fin fct
	
	function addEncadre($objEncadre) {
		$this->liste_encadres[]=$objEncadre;
	}//fin addEncadre
	
	function addDonneesSpecifiques($objDonneesSpec) {
		$this->donnees_specifiques[]=$objDonneesSpec;
	}//fin addEncadre

	function addEncadreRecherche($objEncadreRecherche) {
				$this->liste_encadres_recherche=$objEncadreRecherche;		 
	}//fin addEncadreRecherche
	
	function genererFluxXML()
	{
		$strXml = "<REQUETE>";
		if ($this->structure=="")
		{
			$strXml .= "<STRUCTURE/>";
		} else
		{
			$strXml .= "<STRUCTURE>".htmlentities($this->structure)."</STRUCTURE>";
		}
		if ($this->rubrique=="")
		{
			$strXml .= "<RUBRIQUE/>";
		} else
		{
			$strXml .= "<RUBRIQUE>".htmlentities($this->rubrique)."</RUBRIQUE>";
		}
		if ($this->langue=="")
		{
			$strXml .= "<LANGUE/>";
		} else
		{
			$strXml .= "<LANGUE>".htmlentities($this->langue)."</LANGUE>";
		}
		if ($this->secure=="")
		{
			$strXml .= "<SECURE/>";
		} else
		{
			$strXml .= "<SECURE>".htmlentities($this->secure)."</SECURE>";
		}
		if (is_array($this->liste_encadres)) {
			$strXml .= "<LISTE_ENCADRES>";
			foreach ($this->liste_encadres as $encadre) {
				if ($encadre->titre!=""||$encadre->contenu!="")
				{
					$strXml .= "<ENCADRE>";
					if ($encadre->titre=="")
					{
						$strXml .= "<TITRE/>";
					} else
					{
						$strXml .= "<TITRE>".$encadre->titre."</TITRE>";
					}
					if ($encadre->contenu=="")
					{
						$strXml .= "<CONTENU/>";
					} else
					{
						$strXml .= "<CONTENU>".$encadre->contenu."</CONTENU>";
					}
					$strXml .= "</ENCADRE>";
				}
			}
			$strXml .= "</LISTE_ENCADRES>";
		}
		if (is_array($this->liste_encadres_recherche)) {
			$strXml .= "<LISTE_ENCADRES_RECHERCHE>";
			foreach ($this->liste_encadres_recherche->encadre_recherche as $encadre) {
				if ($encadre!="")
				{
					$strXml .= "<ENCADRE_RECHERCHE>".$encadre."</ENCADRE_RECHERCHE>";
				}
			}
	
			$strXml .= "</LISTE_ENCADRES_RECHERCHE>";
		}
		if (is_array($this->donnees_specifiques)) {
			$strXml .= "<DONNEES_SPECIFIQUES>";
			foreach ($this->donnees_specifiques as $donneeSpecifique) {
				if ($donneeSpecifique->nom!=""||$donneeSpecifique->valeur!="")
				{
					$strXml .= "<DONNEE>";
					if ($donneeSpecifique->nom=="")
					{
						$strXml .= "<NOM/>";
					} else
					{
						$strXml .= "<NOM>".$donneeSpecifique->nom."</NOM>";
					}
					if ($donneeSpecifique->valeur=="")
					{
						$strXml .= "<VALEUR/>";
					} else
					{
						$strXml .= "<VALEUR>".$donneeSpecifique->valeur."</VALEUR>";
					}
					$strXml .= "</DONNEE>";
				}
			}
			$strXml .= "</DONNEES_SPECIFIQUES>";
		}
		$strXml .= "</REQUETE>";
		return $strXml;

	}
}//requete

?>
