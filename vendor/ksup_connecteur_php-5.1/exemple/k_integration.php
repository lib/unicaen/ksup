<?php
	// on peut piloter le code rubrique, la structure, la langue et le mode https
	$strRubrique = "";
	$strStructure = "";
	$objRequete = new requete($strRubrique, $strStructure);
	$langue = $_SESSION["LANGUE"];
	$secure = $_SESSION["SECURE"];
	if (isset($langue))
		$objRequete->setLangue($langue);
	if (isset($secure))
		$objRequete->setSecure("1" == $secure);

	// ajout d'encadr�s (contenu, controles, menus, ...)
	$strTitre = "Encadre 1";
	$strContenu = "contenu_encadre 1";
	$objEncadre1 = new encadre($strTitre,$strContenu);
	$strTitre = "Encadre 2";
	$strContenu = "contenu encadre 2";
	$objEncadre2 = new encadre($strTitre,$strContenu);

	// ajout d'encadr�s de recherche
	$objEncadreRecherche=new liste_encadres_recherche();		
	$objEncadreRecherche->addEncadre_recherche("0002");

	$objRequete->addDonneesSpecifiques($objDonneesSpecifiques);
	$objRequete->addEncadre($objEncadre1);
	$objRequete->addEncadre($objEncadre2);
	$objRequete->addEncadreRecherche($objEncadreRecherche);	

	$includeHead = "<script type=\"text/javascript\" src=\"/js/fonctions.js\"></script>\r\n";
	$includeHead.= "<link rel=\"stylesheet\" type=\"text/css\" media=\"screen\" href=\"/css/styles.css\" title=\"defaut\" />\r\n";

	$objDonneesSpecifiques = new donnesSpecifiques("INCLUDE_HEAD", $includeHead);
	$objRequete->addDonneesSpecifiques($objDonneesSpecifiques); 

	$GLOBALS["objRequete"] = $objRequete;
?>
