<?php
namespace UnicaenKsup;

return [
    'unicaen-ksup' => [
        'connecteur_path' => __DIR__ . '/../vendor/ksup_connecteur_php-5.1/kphplib5/',
        'remplacement' => [
            'charset=iso-8859-1' => 'charset=utf-8'
        ],
    ],

    'view_manager' => [
        'strategies' => [
            'ViewKsupStrategy', // register ksup strategy
        ],
    ],

    'service_manager' => [
        'factories' => [
            'ViewKsupStrategy' => Mvc\Service\ViewKsupStrategyFactory::class,
            'ksup'             => Service\KsupServiceFactory::class,
        ],
    ],
];