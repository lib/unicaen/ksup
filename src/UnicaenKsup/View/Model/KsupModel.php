<?php

namespace UnicaenKsup\View\Model;

use Zend\View\Helper\HeadLink;
use Zend\View\Model\ViewModel;
use Zend\View\Helper\HeadScript;


class KsupModel extends ViewModel
{

    /**
     * @var string
     */
    protected $rubrique = '';

    /**
     * @var string
     */
    protected $structure = '';

    /**
     * @var array
     */
    protected $encadres = [];

    /**
     * @var HeadScript
     */
    protected $scripts;

    /**
     * @var HeadStyle
     */
    protected $links;

    /**
     * @var string
     */
    protected $ssoUrlServer;

    /**
     * @var string
     */
    protected $ssoHostServer;

    /**
     * @var string
     */
    protected $ssoPortServer;

    /**
     * Is this a standalone, or terminal, model?
     *
     * @var bool
     */
    protected $terminate = true;



    public function __construct()
    {
        $this->scripts = new HeadScript();
        $this->links  = new HeadLink();
    }



    /**
     * @return string
     */
    public function getRubrique()
    {
        return $this->rubrique;
    }



    /**
     * @param string $rubrique
     *
     * @return KsupModel
     */
    public function setRubrique($rubrique)
    {
        $this->rubrique = $rubrique;

        return $this;
    }



    /**
     * @return string
     */
    public function getStructure()
    {
        return $this->structure;
    }



    /**
     * @param string $structure
     *
     * @return KsupModel
     */
    public function setStructure($structure)
    {
        $this->structure = $structure;

        return $this;
    }



    /**
     * @param string $titre
     * @param string $contenu
     *
     * @return $this
     */
    public function addEncadre($titre, $contenu)
    {
        if ($contenu) {
            $this->encadres[$titre] = $contenu;
        } else {
            unset($this->encadres[$titre]);
        }

        return $this;
    }



    /**
     * @return array
     */
    public function getEncadres()
    {
        return $this->encadres;
    }



    /**
     * @param array $encadres
     *
     * @return KsupModel
     */
    public function setEncadres(array $encadres = [])
    {
        $this->encadres = $encadres;

        return $this;
    }



    /**
     * @return HeadScript
     */
    public function getScripts()
    {
        return $this->scripts;
    }



    /**
     * @return HeadLink
     */
    public function getLinks()
    {
        return $this->links;
    }



    /**
     * @return string
     */
    public function getSsoUrlServer()
    {
        return $this->ssoUrlServer;
    }



    /**
     * @param string $ssoUrlServer
     *
     * @return KsupModel
     */
    public function setSsoUrlServer($ssoUrlServer)
    {
        $this->ssoUrlServer = $ssoUrlServer;

        return $this;
    }



    /**
     * @return string
     */
    public function getSsoHostServer()
    {
        return $this->ssoHostServer;
    }



    /**
     * @param string $ssoHostServer
     *
     * @return KsupModel
     */
    public function setSsoHostServer($ssoHostServer)
    {
        $this->ssoHostServer = $ssoHostServer;

        return $this;
    }



    /**
     * @return string
     */
    public function getSsoPortServer()
    {
        return $this->ssoPortServer;
    }



    /**
     * @param string $ssoPortServer
     *
     * @return KsupModel
     */
    public function setSsoPortServer($ssoPortServer)
    {
        $this->ssoPortServer = $ssoPortServer;

        return $this;
    }

}