<?php

namespace UnicaenKsup\View\Strategy;

use UnicaenKsup\Service\Traits\KsupServiceAwareTrait;
use Zend\EventManager\AbstractListenerAggregate;
use Zend\EventManager\EventManagerInterface;
use Zend\View\Renderer\PhpRenderer;
use Zend\View\ViewEvent;
use UnicaenKsup\View\Model\KsupModel;

class KsupStrategy extends AbstractListenerAggregate
{
    use KsupServiceAwareTrait;


    /**
     * {@inheritDoc}
     */
    public function attach(EventManagerInterface $events, $priority = 1)
    {
        $this->listeners[] = $events->attach(ViewEvent::EVENT_RENDERER, [$this, 'selectRenderer'], $priority);
        $this->listeners[] = $events->attach(ViewEvent::EVENT_RESPONSE, [$this, 'injectResponse'], $priority);
    }



    /**
     * Detect if we should use the PhpRenderer based on model type and/or
     * Accept header
     *
     * @param  ViewEvent $e
     *
     * @return null|PhpRenderer
     */
    public function selectRenderer(ViewEvent $e)
    {
        $model = $e->getModel();

        if (!$model instanceof KsupModel) {
            // no KsupModel; do nothing
            return;
        }

        // KsupModel found
        return $e->getRenderer();
    }



    /**
     * Inject the response with the XML payload and appropriate Content-Type header
     *
     * @param  ViewEvent $e
     *
     * @return void
     */
    public function injectResponse(ViewEvent $e)
    {
        $result = $e->getResult();
        if (!is_string($result)) {
            // We don't have a string, and thus, no HTML
            return;
        }

        $model = $e->getModel();
        /* @var $model KsupModel */

        if ($model instanceof KsupModel) {
            $this->getServiceKsup()->setModel($model);
            $result = $this->getServiceKsup()->render($result);
            $e->setResult($result);
        }

        // Populate response
        $response = $e->getResponse();
        $response->setContent($result);
    }
}
