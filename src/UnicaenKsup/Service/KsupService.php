<?php

namespace UnicaenKsup\Service;

use UnicaenKsup\View\Model\KsupModel;
use Zend\View\Helper\BasePath;

class KsupService
{

    /**
     * @var KsupModel
     */
    protected $model;

    /**
     * @var array
     */
    protected $config;

    /**
     * @var BasePath
     */
    private $basePath;



    /**
     * @return KsupModel
     */
    public function getModel()
    {
        return $this->model;
    }



    /**
     * @param KsupModel $model
     *
     * @return KsupService
     */
    public function setModel($model)
    {
        $this->model = $model;

        return $this;
    }



    /**
     * @return BasePath
     */
    public function getBasePath()
    {
        return $this->basePath;
    }



    /**
     * @param BasePath $basePath
     *
     * @return KsupService
     */
    public function setBasePath($basePath)
    {
        $this->basePath = $basePath;

        return $this;
    }



    protected function initVars()
    {
        /* Initialisation des variables globales à donner au connecteur KSup */
        define("KPHPLIB_PATH", $this->getConfig('connecteur_path'));
        define("KPHPLIB_PATH_CLASSE", KPHPLIB_PATH . "classe/");
        define("KPHPLIB_PATH_CLASSE_DATA", KPHPLIB_PATH . "classe/data/");
        define("KPHPLIB_PATH_CLASSE_SSO", KPHPLIB_PATH . "classe/sso/");
        define("KPHPLIB_PATH_CLASSE_UTIL_XML", KPHPLIB_PATH . "classe/util/xml/");
        define("KPHPLIB_PATH_CLASSE_UTIL_HTTP", KPHPLIB_PATH . "classe/util/http/");
        define("KPHPLIB_PATH_INCLUDE", KPHPLIB_PATH . "include/");

        include_once(KPHPLIB_PATH_INCLUDE . "params.php");

        define("SSO_URL_SERVER", $this->getModel()->getSsoUrlServer() ?: $this->getConfig('sso_url_server'));
        define("SSO_HOST_SERVER", $this->getModel()->getSsoHostServer() ?: $this->getConfig('sso_host_server'));
        define("SSO_PORT_SERVER", $this->getModel()->getSsoPortServer() ?: $this->getConfig('sso_port_server'));

        return $this;
    }



    /**
     * @return \requete
     */
    protected function getRequete()
    {
        global $GLOBALS;

        if (!isset($GLOBALS["objRequete"])) {
            $rubrique = $this->getModel()->getRubrique();
            if (!$rubrique) $rubrique = $this->getConfig('default-rubrique');

            $structure = $this->getModel()->getStructure();
            if (!$structure) $structure = $this->getConfig('default-structure');

            $GLOBALS["objRequete"] = new \requete($rubrique, $structure);
            if ($langue = $this->getConfig('langue')) $GLOBALS["objRequete"]->setLangue($langue);
            if ($secure = $this->getConfig('https')) $GLOBALS["objRequete"]->setSecure($secure);
        }

        return $GLOBALS["objRequete"];
    }



    protected function traiterEncadres()
    {
        $encadres = $this->getModel()->getEncadres();
        foreach ($encadres as $titre => $contenu) {
            $this->getRequete()->addEncadre(new \encadre($titre, $contenu));
        }

        return $this;
    }



    public function traiterDonneesSpecifiques()
    {
        $dsCode = '';

        $links = $this->getConfig('links');
        if ($links) foreach( $links as $link ){
            $dsCode .=  '<link href="'.$this->makeAbsUrl($link).'" media="all" rel="stylesheet" type="text/css">'."\r\n";
        }
        $dsCode .= trim($this->getModel()->getLinks()->toString(4));

        $scripts = $this->getConfig('scripts');
        if ($scripts) foreach( $scripts as $script ){
            $dsCode .=  '<script type="text/javascript" src="'.$this->makeAbsUrl($script).'"></script>'."\r\n";
        }
        $dsCode .= trim($this->getModel()->getScripts()->toString(4));
//        if ($dsCode) {
//            $ds = new \donnesSpecifiques("INCLUDE_HEAD", $dsCode);
//            $this->getRequete()->addDonneesSpecifiques($ds);
//        }

        return $dsCode;
    }



    private function makeAbsUrl( $url )
    {
        if (0 !== strpos($url, '//') && 0 !== strpos($url, 'http://') && 0 !== strpos($url, 'https://')) {
            $url = $this->basePath->__invoke($url);
        }

        return $url;
    }



    protected function lireTemplate($template)
    {
        $patterns = $this->getConfig('remplacement');

        ob_start();
        \connecteurMgr::lireTemplate($template);
        $res = ob_get_contents();
        ob_end_clean();
        foreach ($patterns as $old => $new) {
            $res = str_replace($old, $new, $res);
        }
        $res = trim(utf8_encode($res));

        return $res;
    }



    public function render($content)
    {
        if (!$this->getModel()) {
            throw new \Exception('Pas de modèle transmis!!');
        }

        $this->initVars();
        $this->getRequete();

        /* Contrôle de session! */
        $bean = new \sso();
        if (array_key_exists("kticket", $_GET) && $_SESSION["KSESSION"] == "") {
            \connecteurMgr::validerTicket($bean);
        } else {
            \connecteurMgr::verifierSession($bean);
        }

        $this->traiterEncadres();
        //$this->traiterDonneesSpecifiques(); // ne fonctionne pas avec KSUP!!

        $haut = $this->lireTemplate('haut');

        $bodyPos  = strpos($haut, '</head>');
        $insert = $this->traiterDonneesSpecifiques() . "\r\n";
        $haut = substr($haut, 0, $bodyPos) . $insert . substr($haut, $bodyPos);

        $bas = $this->lireTemplate('bas');

        return $haut . $content . $bas;
    }



    public function getConfig($key = null)
    {
        $config = $this->config;

        if ($key) {
            $key = explode('/', $key);

            foreach ($key as $k) {
                if (isset($config[$k])) {
                    $config = $config[$k];
                } else {
                    return null;
                }
            }
        }

        return $config;
    }



    public function setConfig(array $config)
    {
        $this->config = $config;
    }

}