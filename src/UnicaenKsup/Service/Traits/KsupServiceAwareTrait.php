<?php

namespace UnicaenKsup\Service\Traits;

use UnicaenKsup\Service\KsupService;
use RuntimeException;

/**
 * Description of KsupServiceAwareTrait
 *
 * @author UnicaenCode
 */
trait KsupServiceAwareTrait
{
    /**
     * @var KsupService
     */
    private $serviceKsup;





    /**
     * @param KsupService $serviceKsup
     * @return self
     */
    public function setServiceKsup( KsupService $serviceKsup )
    {
        $this->serviceKsup = $serviceKsup;
        return $this;
    }



    /**
     * @return KsupService
     * @throws RuntimeException
     */
    public function getServiceKsup()
    {
        if (empty($this->serviceKsup)){
            if (! method_exists($this, 'getServiceLocator')) {
                throw new RuntimeException( 'La classe '.get_class($this).' n\'a pas accès au ServiceLocator.');
            }

            $serviceLocator = $this->getServiceLocator();
            if (method_exists($serviceLocator, 'getServiceLocator')) {
                $serviceLocator = $serviceLocator->getServiceLocator();
            }
            $this->serviceKsup = $serviceLocator->get('ksup');
        }
        return $this->serviceKsup;
    }
}