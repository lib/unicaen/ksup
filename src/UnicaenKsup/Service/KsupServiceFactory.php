<?php

namespace UnicaenKsup\Service;

use Interop\Container\ContainerInterface;
use UnicaenKsup\View\Strategy\KsupStrategy;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\View\Helper\BasePath;

class KsupServiceFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return object|KsupService
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new KsupService();

        $config = $container->get('config');
        if (isset($config['unicaen-ksup'])) {
            $config = $config['unicaen-ksup'];
        } else {
            $config = [];
        }
        $service->setConfig($config);

        /** @var BasePath $basePath */
        $basePath = $container->get('ViewHelperManager')->get('basePath');
        $service->setBasePath($basePath);

        return $service;
    }

    public function createService(ServiceLocatorInterface $container)
    {
        return $this($container, KsupService::class);
    }
}
