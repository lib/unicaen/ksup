<?php

namespace UnicaenKsup\Mvc\Service;

use Application\View\Renderer\PhpRenderer;
use Interop\Container\ContainerInterface;
use UnicaenKsup\Service\KsupService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use UnicaenKsup\View\Strategy\KsupStrategy;

class ViewKsupStrategyFactory implements FactoryInterface
{

    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return object|KsupStrategy
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var PhpRenderer $renderer */
        $renderer = $container->get('ViewRenderer');

        /** @var KsupService $service */
        $service = $container->get('ksup');

        $strategy = new KsupStrategy($renderer);
        $strategy->setServiceKsup($service);
        return $strategy;
    }

    public function createService(ServiceLocatorInterface $container)
    {
        return $this($container, KsupStrategy::class);
    }
}